#pragma once

#include <vector>
#include <string>
#include <unordered_map>

class Labryinth {
    int _n, _m;
    static char const hole = '.';
    std::vector<std::string> _input, _result;
    std::string _schema;
    int longest_path_length;

// Utility functions.
    int compute_longest_path_util (int i, int j,
        std::vector<std::vector<int>>&,
        std::vector<std::vector<bool>>&,
        std::unordered_map<int, int> &);
    
    bool bound (int i, int j);
    
public:
    Labryinth (std::vector <std::string> & input)
    : _input (input), _result (input), _n (input.size()), _m (input.begin()->size()), _schema ("") {}
    Labryinth () {}

    void compute_longest_path ();
    int get_longest_path_length () const;
    std::string get_longest_path_schema () const;

    friend std::istream& operator>> (std::istream &, Labryinth &);
};

class IllegalFormat : public std::exception {
    std::string _msg;
public:
    IllegalFormat (const char * what)
        : _msg (what) {}

    const char * what() const noexcept override {return _msg.c_str();}
};
