# Instructions to build
```
mkdir build
cd build
cmake ..
make
cd ..
```

# Instructions to run
```
bin/run <input-file>
```
The &lt;input-file&gt; contains the labryinth in the specified format. The /tests folder contains many test cases.

# Output format
The first line indicates the length of the path.
The next lines contain the schema of the largest path.

# Approach
The approach I took was exhaustive search. For each cell, I compute the longest path starting from that cell using a dynamic programming approach. My equation for dynamic programming is:

```
if ([i,j] is WALL) then OPT[i,j] = 0
else
    OPT[i,j] = max (OPT[x,y]) for [x,y] in neighbors of [i,j] 
    (such that the longest path from the neighbors does not cut [i,j])
```
The dynamic programming algorithm will take O(grid size) in the worst case.

Since this is done for every 'HOLE' this will take O((grid_size)^2).