cmake_minimum_required(VERSION 2.8)
project(Labryinth)

set(CMAKE_CXX_STANDARD 11)

set (SRC_DIR "src")
set (INC_DIR "include")
set(EXECUTABLE_OUTPUT_PATH "../bin")

include_directories (${INC_DIR})

add_executable(run
        ${SRC_DIR}/Labryinth.cpp
        ${SRC_DIR}/main.cpp
    )