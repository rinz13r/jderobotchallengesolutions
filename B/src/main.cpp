#include "Labryinth.h"

#include <iostream>
#include <fstream>

int main (int argc, char ** argv) {
    if (argc != 2) {
        std::cerr << "Usage: ./a.out <input-file>\n";
        return 1;
    }
    std::ifstream ifs (argv[1]);
    if (!ifs.good()) {
        std::cerr << "No such file!\n";
        return 1;
    }
    Labryinth l;
    try {
        ifs >> l;
    } catch (std::exception const& e) {
        std::cerr << e.what() << '\n';
        std::exit (EXIT_FAILURE);
    }

    l.compute_longest_path();
    std::cout << l.get_longest_path_length() << '\n';
    std::cout << l.get_longest_path_schema() << '\n';

    return 0;
}