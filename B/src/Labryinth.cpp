#include "Labryinth.h"

#include <iostream>
#include <map>
#include <vector>

static char const HOLE = '.', WALL = '#';

std::string Labryinth::get_longest_path_schema () const {return _schema;}
int Labryinth::get_longest_path_length () const {return longest_path_length;}

int Labryinth::compute_longest_path_util (int i , int j, std::vector<std::vector<int>>& mem, std::vector<std::vector<bool>>& mark, std::unordered_map<int, int> & next) {
	if (mem[i][j] != -1) {return mem[i][j];}
	int m = 0, par = i*_m+j;
	mark[i][j] = true;
	for (int y = -1; y <= 1; y++) {
		for (int x = -1; x <= 1; x++) {
			if (x*y != 0) {continue;}
			if (bound (i+y, j+x) && !mark[i+y][j+x]) {
				int mt = compute_longest_path_util (i+y, j+x, mem, mark, next);
				if (mt > m) {
					m = mt;
					par = (i+y)*_m+j+x;
				}
			}
		}
	}
	mark[i][j]=false;
	next[i*_m+j] = par;

	return mem[i][j] = (m+1);
}

bool Labryinth::bound (int i, int j) {
	return (i<= _n-1 && i >= 0 && j >= 0 && j <= _m-1);
}

void Labryinth::compute_longest_path () {
	int n = _n, m = _m;
	std::vector<std::vector<int>> mem; //mem[i][j] is the longest path starting form (i,j).
	std::vector<std::vector<bool>> mark;
	for (int i = 0; i < n; i++) {
		mem.push_back(std::vector<int>(m));
		mark.push_back(std::vector<bool>(m, false));
		for (int j = 0; j < m; j++) {
			if (_input.at(i).at(j)== WALL) {mem[i][j] = (0);} // if it's a wall, then we know that we can't cross it, so set it to 0.
			else {mem[i][j] = (-1);} // Else, it needs to be computed hence, set to -1.
		}
	}
	auto memcopy = mem;
	auto markcopy= mark;
	int res = 0;
	std::pair<int, int> st;
	std::unordered_map<int, int> next, res_next; // next stores where should we go from (i, j) to get the longest path.
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			if (mem[i][j] == -1) {
				int ans = compute_longest_path_util (i, j, mem, mark, next);
				if (ans > res) {
					st = std::make_pair (i, j);
					res = ans;
					res_next = next;
				}
				next.clear();
				mem = memcopy;
				mark=markcopy;
			}
		}
	}
// Update the result.
	longest_path_length = res;
	if (res == 0) {; /* Do not update the result.*/}
	else if (res == 1) { _result[st.first][st.second] = '0';}
	else {
		int curr = st.first * _m + st.second;
		int id = 0;
		while (true) {
			_result[curr/m][curr%m] = '0'+id++;
			if (res_next[curr] == curr) {break;}
			curr = res_next[curr];
		}
	}

//Write to the schema
	for (auto & e : _result) { _schema += (e+'\n');}
}

std::istream& operator>> (std::istream& is, Labryinth & l) {
    std::string s;
    while (true) {
        std::getline (is, s);
        if (s.empty()) break;
        l._input.push_back (s);
        s.clear();
    }
    if (l._input.size() != 0) {
        l._m = l._input.begin()->size();
        for (auto & e : l._input) {
            if (e.size() != l._m) {
                throw IllegalFormat ("Unexpected row length.");
            }
        }
    } else {l._m = 0;}
    l._n = l._input.size();
    l._result = l._input;
    
    return is;
}