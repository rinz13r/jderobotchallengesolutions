import sys
from Life.load_grid import load_grid

if __name__ == '__main__':
    if (len(sys.argv) != 2):
        print ("Usage : python3 main.py <filename>")
        exit()
    grid = load_grid (sys.argv[1])
    grid.run()