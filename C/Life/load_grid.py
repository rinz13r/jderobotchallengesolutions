import json
from Life import grid

# Loads a grid from json file
def load_grid (filename : str):
    with open (filename) as f:
        data=json.load(f)
    d = data['grid']
    n, m = d['n'], d['m']
    alive_cells = d['alive_cells']
    roff, coff = d['r_offset'], d['c_offset']
    
    g = [['.' for i in range (0, m)] for j in range (0, n)]
    for p in alive_cells:
        g[p[0]+roff][p[1]+coff] = '#'

    return grid.Grid (n, m, g)
