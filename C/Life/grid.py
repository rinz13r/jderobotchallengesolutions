import os
import sys
import json

class Grid:
    CELL = '#'
    NONE = '.'
    def __init__(self, _n, _m, _grid):
        self.n, self.m = _n, _m
        self.grid = _grid

    def n_neighbors (self, point):
        (y, x) = point
        n_nbrs = 0
        for i in range (-1, 2):
            for j in range (-1, 2):
                if (i==0 and j==0):
                    continue
                if (self.bound ((y+i,x+j)) and self.grid[y+i][x+j] == self.CELL):
                    n_nbrs += 1
        return n_nbrs

    def bound (self, point):
        (y, x) = point
        return (y>=0 and y<self.n and x>=0 and x<self.m)
    
    def update (self):
        toupdate = []
        for i in range (0, self.n):
            for j in range (0, self.m):
                n_nbrs = self.n_neighbors ((i, j))
                if (self.grid[i][j] == self.CELL):
                    if (n_nbrs < 2 or n_nbrs > 3):
                        toupdate.append (((i,j), Grid.NONE))
                else:
                    if (n_nbrs == 3):
                        toupdate.append(((i,j), Grid.CELL))
        for e in toupdate:
            self.grid[e[0][0]][e[0][1]] = e[1]
    
    def run (self):
        r = False
        while (True):
            self.render()
            self.update()
            if (r == False):
                x = input()
                if (x == 'q' or x == 'Q'):
                    break 
                elif (x == 'r' or x == 'R'):
                    r = True

    def render (self):
        os.system('clear')
        for i in range (0, self.n):
            for j in range (0, self.m):
                sys.stdout.write (self.grid[i][j])
            sys.stdout.write ('\n')
            sys.stdout.flush()
