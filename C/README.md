# Instructions to run
```
python3 main.py <input-file>
```
1. Everything is printed to the terminal.
2. For transission to the next generation to occur, press 'ENTER'
3. To quit, input 'q'
4. To continuously run, input 'r' or 'R'
5. To exit from continuous input, give the interrupt Ctrl+C.

# Rules
1. Any live cell with less than 2 or greater than 3 alive neighbors, dies.
2. Any live cell with exactly 2 or 3 alive neighbors, lives.
3. Any dead cell with exactly 3 neighbors becomes alive.

(Taken from https://en.wikipedia.org/wiki/Conway's_Game_of_Life#Rules)

# Json format
CELL : '#'<br>
NONE : '.'<br>
The "grid" consists of the following:
1. "n" : Number of columns
2. "m" : Number of rows.
3. "r_offset" : The row offset from which the initial alive CELLs are placed.
4. "c_offset" : The column offset from which the initial alive CELLs are placed.
5. "alive_cells" : The row and column number pairs of cells that are alive to begin with.

# References
https://en.wikipedia.org/wiki/Conway's_Game_of_Life#Rules